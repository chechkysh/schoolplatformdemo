# SchoolPlatformDemo

Здравствуйте! Это **демонстративная** версия одного из моих проектов. Часть кода и данных скрыта по желанию работодателя. Тут вы можете ознакомиться с моим стилем написания кода и с частью моих навыков. **Сам проект лежит на ветке master**

### О проекте:


Это бекенд для информационной платформы, которая должна агрегировать в себе данные о большом количестве школ, лагерей и олимпиад по математике и программированию

Платформа нужна для быстрого и простого поиска информации о вышеперечисленных мероприятиях и школах. Также платформа предоставляет возможность отслеживать новости о определенных мероприятиях и школах путем подписки на них. 


*Функционал:*

* Предоставление данных о различных школах, лагерях и олимпиадах 
* Подписка на новости конкретной школы
* Оповещение подписавшихся о вступительных экзаменах, олимпиадах и др
* Настройка получаемых оповещений (вкл/выкл определенный тип)


*Стек технологий:*

* Python
* Django Rest Framework
* PostgreSQL
* Docker
* Nginx
* aiogram и другие асинхронные библиотеки
* Celery
* celery-beat
* Redis
